import threading
import time
import re


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Car(threading.Thread, metaclass=Singleton):
    """
    Кабина лифта.

    Логика работы:
    1. Если лифт едет в каком-то направлении, то надо ехать в этом направлении как можно дальше
    1.1. Пока не закончатся команды (выполняя в порядке направления)
    1.2. Подбирем по пути людей, если они хотят ехать в том же направлении
    2. По окончании движения по направлению, двигаемся в другом направлении, если есть команды
    3. Если команд нет, едем на 1 этаж
    4. Если лифт пустой, при перемещении на 1 этаж, двери не открываем
    """

    def __init__(self, speed, wait_time, stages, current_stage=1):
        """
        Create car instance
        :param speed:
        :param wait_time:
        :param stages:
        :param current_stage:
        """

        # threading
        threading.Thread.__init__(self)
        self._stop_event = threading.Event()

        # settings
        self.speed = speed
        self.wait_time = wait_time
        self.max_stage = stages

        # states
        self.current_stage = current_stage
        self.current_status = "Doors are closed on " + str(current_stage)
        self.direction = None
        self.destination = False
        self.go_home = False

        # queue
        self.queue = {"up": [], "down": []}

    def run(self):
        """
        On start car thread
        :return:
        """
        while not self.destroyed():
            if self.queue["up"] or self.queue["down"]:
                self.set_direction()
                self.set_destination()
                self.move_stage()
                if self.current_stage is self.destination:
                    self.open_close()
                    self.complete_stage()
            else:
                self.wait_for_command()

    def set_direction(self):
        """
        Set direction
        :return:
        """

        direction = self.direction

        # Если нет направления, едем туда, где есть пункты назначения
        if not direction:
            if self.queue["up"]:
                direction = "up"
            elif self.queue["down"]:
                direction = "down"
            else:
                raise RuntimeError("Cant understand direction")
        else:
            # Если по текущему направлению нету пунктов назначения
            if not self.queue[direction]:

                # Определим первый пункт назначения в противоположном направлении
                # Если едем в ту же сторону - добавляем пункт назначения
                opposite_direction = "down" if direction is not "down" else "up"
                direction_queue = self.queue[opposite_direction]

                if direction is "up":
                    opposite_first_destination = max(direction_queue)
                    if opposite_first_destination > self.current_stage:
                        self.add_stage(opposite_first_destination, direction)
                    else:
                        direction = "down" if direction is not "down" else "up"
                elif direction is "down":
                    opposite_first_destination = min(direction_queue)
                    if opposite_first_destination < self.current_stage:
                        self.add_stage(opposite_first_destination, direction)
                    else:
                        direction = "down" if direction is not "down" else "up"
                else:
                    raise RuntimeError("Cant understand destination!")

        self.direction = direction

    def set_destination(self):
        """
        Set destination
        :return:
        """

        direction = self.direction
        destination = self.destination

        if direction is not "up" and not "down":
            raise RuntimeError("Cant understand destination!")

        # Если нет пункта назначения, выбираем тот что есть по направлению
        if not destination:
            direction_queue = self.queue[direction]
            if direction is "up":
                destination = min(direction_queue)
            elif direction is "down":
                destination = max(direction_queue)
        else:
            # Если направление удалено из списка, мы его прошли. Найдем следующее назначение
            # Всгда находим ближайшее к текущему этажу назначение (подхватываем попутчиков)
            direction_queue = self.queue[direction]
            if destination not in self.queue[direction]:
                if direction is "up":
                    destination = self.limited_min(direction_queue, self.current_stage)
                elif direction is "down":
                    destination = self.limited_max(direction_queue, self.current_stage)

            # Если есть новый более ближний пункт назначения по текущему направлению,
            # меняем на него пункт назначения
            # (если направление - вверх, не меньше текущего этажа, если вниз - не больше)
            new_destination = destination
            for st in direction_queue:
                if direction is "up":
                    if new_destination > st > self.current_stage:
                        new_destination = st
                elif direction is "down":
                    if new_destination < st < self.current_stage:
                        new_destination = st
            destination = new_destination

        self.destination = destination

    def move_stage(self):
        """
        Move car for 1 stage
        :return:
        """
        if self.destination > self.current_stage:
            sign = 1
        elif self.destination < self.current_stage:
            sign = -1
        else:
            sign = 0
        time.sleep(self.speed)
        self.current_stage = self.current_stage + sign
        self.current_status = "Moving on " + str(self.current_stage)

    def open_close(self):
        """
        Open doors, wait, close doors
        :return:
        """
        # Если лифт приехал на 1 этаж пустой, можно двери не открывать
        if self.go_home and self.current_stage is 1:
            pass
        else:
            self.open_the_doors()
            self.wait_opened()
        self.close_the_doors()
        return True

    def complete_stage(self):
        """
        Complete stage - rm destination from queue, restore statuses
        :return:
        """

        self.queue[self.direction].remove(self.destination)
        opposite_direction = "down" if self.direction is not "down" else "up"
        if self.destination in self.queue[opposite_direction]:
            self.queue[opposite_direction].remove(self.destination)

        # Если нет пункта назначения и мы не на 1 этаже - возвращаем лифт на 1 этаж
        if not self.queue["up"] and not self.queue["down"] and self.current_stage is not 1:
            self.add_stage(1, "down")
            self.go_home = True

        # Если лифт дома, сбросим статусы
        if self.current_stage is 1 and self.go_home:
            self.go_home = False
            self.destination = False
            self.direction = None

        return True

    def add_stage(self, stage, direction, outer=False):
        """
        :param outer:
        :param stage:
        :param direction:
        :return:
        """
        self.go_home = False
        if not self.stage_valid(stage):
            self.current_status = "Wrong stage"
        else:
            stage = int(stage)
            # Определим, куда хочет человек, нажимая кнопку изнутри - вниз или вверх
            if not outer:
                if stage > self.current_stage:
                    direction = "up"
                elif stage < self.current_stage:
                    direction = "down"
                else:
                    return
            if not self.already_added(stage, direction):
                self.prepend_queue(stage, direction)

    def stage_valid(self, stage):
        """
        Stage validation
        :param stage:
        :return:
        """
        if not type(stage) == int:
            stage = re.sub("[^0-9]", "", stage)
            if stage is "":
                return False
            stage = int(stage)
        if stage <= 0 or stage > self.max_stage:
            return False
        return True

    def already_added(self, stage, direction):
        """
        Check if user clicked button lots of time
        :param direction:
        :param stage:
        :return:
        """
        direction_queue = self.queue[direction]
        if direction_queue:
            if stage in direction_queue:
                return True
        return False

    def wait_opened(self):
        """
        :return:
        """
        time.sleep(self.wait_time)

    def open_the_doors(self):
        """
        :return:
        """
        self.current_status = "Doors are open on " + str(self.current_stage)

    def close_the_doors(self):
        """
        :return:
        """
        self.current_status = "Doors are closed on " + str(self.current_stage)

    @staticmethod
    def wait_for_command():
        """
        :return:
        """
        time.sleep(0.1)
        pass

    def prepend_queue(self, stage, direction):
        """
        Add stage to the queue
        :param direction:
        :param stage:
        :return:
        """
        self.queue[direction] = [stage] + self.queue[direction]

    def stop(self):
        """
        :return:
        """
        self._stop_event.set()

    def destroyed(self):
        """
        :return:
        """
        return self._stop_event.is_set()

    @staticmethod
    def limited_min(list, mn):
        """
        Find lowest value in list but not lower than mn
        :param list:
        :param mn:
        :return:
        """
        available_list = []
        for st in list:
            if st > mn:
                available_list.append(st)
        if not available_list:
            return list[0]
        return min(available_list)

    @staticmethod
    def limited_max(list, mx):
        """
        Find max value in list but not bigger than mx
        :param list:
        :param mx:
        :return:
        """
        available_list = []
        for st in list:
            if st < mx:
                available_list.append(st)
        if not available_list:
            return list[0]
        return max(available_list)
