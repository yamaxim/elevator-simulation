from optparse import OptionParser, OptionValueError
from gui import Gui
from car import Car


class Elevator(Gui):
    """
    Responsible for all elevator system
    """

    def __init__(self):

        # Settings
        self.settings = self.set_up()

        # Car
        speed = self.calculate_speed()
        self.car = Car(speed, self.settings.wait_time, self.settings.stages)
        self.car.start()

        # show GUI
        super().__init__()

    @staticmethod
    def set_up():
        """
        Setting up the elevator
        """

        def check_stages_parameter(option, opt, value, parser):
            """
            Callback to validate stage param
            :param option:
            :param opt:
            :param value:
            :param parser:
            :return:
            """
            if value < 5 or value > 20:
                raise OptionValueError("From 5 to 20 only")
            parser.values.stages = value

        parser = OptionParser()
        parser.add_option(
            "--stages",
            type=int,
            dest="stages",
            help="Stages count",
            default=15,
            action="callback",
            callback=check_stages_parameter
        )
        parser.add_option(
            "--stage_height",
            type=float,
            dest="stage_height",
            help="Stage height (m.)",
            default=3
        )
        parser.add_option(
            "--speed",
            type=float,
            dest="speed",
            help="Speed (m. per sec.)",
            default=1
        )
        parser.add_option(
            "--wait_time",
            type=float,
            dest="wait_time",
            help="Wait time (sec.)",
            default=1
        )
        (options, args) = parser.parse_args()
        return options

    def calculate_speed(self):
        """
        Calc speed for car
        :return:
        """
        return self.settings.stage_height * self.settings.speed
