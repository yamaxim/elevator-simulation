from tkinter import messagebox
from tkinter.ttk import Notebook
from innerframe import InnerFrame
from outerframe import OuterFrame
from tkinter import *


class Gui(Tk):
    """
    Gui of main frame app
    """

    def __init__(self):
        Tk.__init__(self)
        self.protocol("WM_DELETE_WINDOW", lambda: self.close_application())
        self.notebook = Notebook(self)
        self.panel = InnerFrame(padx=30, pady=30)
        self.build_interface()

    @staticmethod
    def update_outer_panel(elevator, frame, stage=1):
        """
        Update outer panel onclick
        :param elevator:
        :param frame:
        :param stage:
        :return:
        """
        if Gui.stage_valid(stage):
            frame.forget()
            frame.destroy()
            elevator.panel = OuterFrame(stage, elevator.settings.stages, padx=30, pady=30)
            elevator.notebook.add(elevator.panel, text='Your panel')
            elevator.notebook.select(elevator.panel)

    @staticmethod
    def update_inner_panel(elevator, frame):
        """
        Update inenr panel onclick
        :param elevator:
        :param frame:
        :return:
        """
        frame.forget()
        frame.destroy()
        elevator.panel = InnerFrame(padx=30, pady=30)
        elevator.notebook.add(elevator.panel, text='Your panel')
        elevator.notebook.select(elevator.panel)

    @staticmethod
    def stage_valid(stage):
        """
        Stage validation
        :param stage:
        :return:
        """
        stage = re.sub("[^0-9]", "", stage)
        if stage is "":
            messagebox.showinfo("Error", "Empty stage")
            return False
        stage = int(stage)
        if stage > 20:
            messagebox.showinfo("Error", "Wrong stage")
            return False
        return True

    def close_application(self):
        """
        :return:
        """
        self.car.stop()
        self.destroy()

    def build_interface(self):
        """
        Build main app frame
        :return:
        """
        self.resizable(False, False)
        self.geometry('300x300')
        self.title('Elevator')
        self.panel = OuterFrame(1, self.settings.stages, padx=30, pady=30)
        teleport_frame = Frame(self, padx=30, pady=30)
        self.notebook.add(teleport_frame, text='Where are you?')
        self.notebook.add(self.panel, text='Your panel')
        self.notebook.pack(fill=BOTH)
        # teleport
        input_your_stage_label = StringVar()
        input_your_stage_label.set("Which stage are you staying?")
        input_your_stage_label_dir = Label(teleport_frame, textvariable=input_your_stage_label, height=1)
        input_your_stage_label_dir.pack(fill="x")
        default_value = IntVar(self, value=1)
        input_your_stage = Entry(teleport_frame, textvariable=default_value)
        input_your_stage.pack(fill=BOTH)
        input_your_stage.focus_set()
        replace_button = Button(
            teleport_frame,
            text='Replace me',
            command=lambda: self.update_outer_panel(self, self.panel, input_your_stage.get()),
            height=2,
            bg='#CCC'
        )
        replace_button.pack(fill="x")
        in_label = StringVar()
        in_label.set("Or you want to get into the car?")
        in_label_dir = Label(teleport_frame, textvariable=in_label, height=1)
        in_label_dir.pack(fill="x")
        in_button = Button(
            teleport_frame,
            text='Let me in',
            command=lambda: self.update_inner_panel(self, self.panel),
            height=2,
            bg='#CCC'
        )
        in_button.pack(fill="x")
        def task():
            self.panel.show_status()
            self.after(100, task)
        self.after(100, task)
        self.mainloop()
