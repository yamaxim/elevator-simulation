from tkinter import *
from panel import Panel


class InnerFrame(Panel):
    """
    You can see this panel GUI if you are inside the car
    """

    def __init__(self, **kw):
        """
        :param kw:
        """
        super().__init__(**kw)
        self.build_panel()

    def build_panel(self):
        """
        :return:
        """
        current_stage_text_label = StringVar()
        current_stage_text_label.set("You are in the car.")
        current_stage_text_label_dir = Label(self, textvariable=current_stage_text_label, height=1)
        current_stage_text_label_dir.pack(fill="x")
        current_status_text_label = StringVar()
        current_status_text_label.set("Current status is:")
        current_status_text_label_dir = Label(self, textvariable=current_status_text_label, height=1)
        current_status_text_label_dir.pack(fill="x")
        status_label_dir = Label(self, textvariable=self.current_status_label, height=2, bg='#000')
        status_label_dir.pack(fill="x")
        status_label_dir.config(font=("Courier", 12), fg="red")
        input_stage_label = StringVar()
        input_stage_label.set("Witch stage do you want to go?")
        input_stage_label_dir = Label(self, textvariable=input_stage_label, height=1)
        input_stage_label_dir.pack(fill="x")
        input_stage = Entry(self)
        input_stage.pack(fill="x")
        input_stage.focus_set()
        button_call_car = Button(
            self,
            text='Elevator call',
            command=lambda: self.call_car_callback(input_stage.get(), "direct"),
            height=2,
            bg='#CCC'
        )
        button_call_car.pack(fill="x")
