from tkinter import *
from panel import Panel


class OuterFrame(Panel):
    """
    You can see this panel GUI if you are outside the car
    """

    def __init__(self, stage, max, **kw):
        """
        :param stage:
        :param kw:
        """
        super().__init__(**kw)
        self.stage = int(stage)
        self.max = int(max)
        self.current_status_label = StringVar()
        self.build_panel()

    def build_panel(self):
        """
        :return:
        """
        current_stage_text_label = StringVar()
        current_stage_text_label.set("You are outside the car on " + str(self.stage) + " stage.")
        current_stage_text_label_dir = Label(self, textvariable=current_stage_text_label, height=1)
        current_stage_text_label_dir.pack(fill="x")
        current_status_text_label = StringVar()
        current_status_text_label.set("Current status is:")
        current_status_text_label_dir = Label(self, textvariable=current_status_text_label, height=1)
        current_status_text_label_dir.pack(fill="x")
        status_label_dir = Label(self, textvariable=self.current_status_label, height=2, bg='#000')
        status_label_dir.pack(fill="x")
        status_label_dir.config(font=("Courier", 12), fg="red")

        if not self.stage == self.max:
            button_call_car_up = Button(
                self,
                text='Up',
                command=lambda: self.call_car_callback(self.stage, "up", True),
                height=2, bg='#CCC'
            )
            button_call_car_up.pack(fill="x")

        if not self.stage == 1:
            button_call_car_down = Button(
                self, text='Down',
                command=lambda: self.call_car_callback(self.stage, "down", True),
                height=2, bg='#CCC'
            )
            button_call_car_down.pack(fill="x")

