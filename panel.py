from tkinter import *
from car import Car


class Panel(Frame):
    """
    Base class for both panels
    """

    def __init__(self, **kw):
        """
        :param kw:
        """
        super().__init__(**kw)
        self.current_status_label = StringVar()

    @staticmethod
    def call_car_callback(stage, direction, outer=False):
        """
        :param outer:
        :param stage:
        :param direction:
        :return:
        """
        car = Car()
        car.add_stage(stage, direction, outer)

    def show_status(self):
        """
        :return:
        """
        car = Car()
        self.current_status_label.set(car.current_status)
